network_analystics
=======

Scripts for large scale network analytics

------------------------------------------------------------
* PREREQUISITE
------------------------------------------------------------
All are pig/shell scripts. Requiring Pig >= 0.10.0. 
So far, only tested on Pig 0.10.0. 

------------------------------------------------------------
* GRAPH INPUT FORMAT
------------------------------------------------------------
In all the scripts, we assume the graph is stored in tab-seperated pairs/triplets as

src \t des \t weight

the weight is nevered used in the script. So it is fine to provide just pairs rather thn triplets.


For undirected graph, an edge <u,v> should be stored just once, i.e., only <u, v> or <v,u> appear in the graph data, but not both. 

------------------------------------------------------------
* UTILITIES
------------------------------------------------------------
Below are major scripts to compute the stats:

compute_graph_stats_for_directed_graph.pig:
  compute degree distribution, in/out degree correlation, degree association for directed graphs

compute_graph_stats_for_undirected_graph.pig:
  compute degree distribution, degree association, number of triangles, and clustering coefficients

wcc/RUN_compute_wcc
  iteratively doing label propagation to find weakly connected
  components. Can apply to both DIRECTED/UNDIRECTED graph. 
  Has to run under wcc directory

shortestpath/RUN_shortest_path
  similar to the wcc script. Sample a portion of nodes as seeds and
  find out the shortest path distance from seeds to other nodes. 
  The distribution of path lengths is stored at the end. 
  This script applies to DIRECTED graph only. 
  Has to run under shortestpath directory

Caveat:
  1. All intermediate results of the iterative mapreduce are stored on
  hdfs. For example, iteration 7's result will be stored in i7.  
  Please delete intermediate result if necessary. 
  2. The number of reudcers are not stored in any of the
  script. Mannully adjusting #reducers based on your cluster capacity
  may help improve the speed running the script. 

------------------------------------------------------------
Lasted updated by Lei Tang on 11/24/2013
------------------------------------------------------------

