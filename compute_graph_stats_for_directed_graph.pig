-- given directed graph in triplets, compute out its related graph statistics
-- assuming graph are stored on hdfs in triplets <ID1>, <ID2>, <value>
%default GRAPH 'in/graph';
%default OUT 'out';

GRAPH = load '$GRAPH' as (src:chararray, des:chararray, value:int);

------------------------------------------------------------
-- compute metrics about degrees
------------------------------------------------------------

-- in degree distribution
X = group GRAPH by des;
IN_DEGREE = foreach X generate group as node,  COUNT(GRAPH) as in_degree;
store IN_DEGREE into '$OUT/in_degree';

-- out degree distribution
X = group GRAPH by src;
OUT_DEGREE = foreach X generate group as node, COUNT(GRAPH) as out_degree;
store OUT_DEGREE INTO '$OUT/out_degree';

-- compute correlation between in_degree and out_degree
X = join IN_DEGREE by node FULL OUTER,  OUT_DEGREE by node;

split X into
    A if IN_DEGREE::node is null and OUT_DEGREE::node is NOT null,  
    B if IN_DEGREE::node is NOT null and OUT_DEGREE::node is null,
    C if IN_DEGREE::node is NOT null and OUT_DEGREE::node is NOT null;

A = foreach A generate
    OUT_DEGREE::node       as node,
    0                      as in_degree,
    OUT_DEGREE::out_degree as out_degree;

B = foreach B generate
    IN_DEGREE::node      as node,
    IN_DEGREE::in_degree as in_degree,
    0                    as out_degree;

C = foreach C generate
    IN_DEGREE::node        as node,
    IN_DEGREE::in_degree   as in_degree,
    OUT_DEGREE::out_degree as out_degree;

store C into '$OUT/nodes_with_nonzero_in_out_degree';


IN_OUT_DEGREE = union A, B, C;
IN_OUT_DEGREE = filter IN_OUT_DEGREE by node is not null;
store IN_OUT_DEGREE into '$OUT/in_out_degree';

-- AS CORRELATION requires input to be double
DEGREES = foreach IN_OUT_DEGREE GENERATE (DOUBLE)in_degree, (DOUBLE)out_degree;
X = group DEGREES all;
DEGREE_CORRELATION = foreach X generate COR(DEGREES.in_degree, DEGREES.out_degree);
store DEGREE_CORRELATION into '$OUT/degree_correlation';


------------------------------------------------------------
-- compute degree association
------------------------------------------------------------

-- join src by in-degree out-degree
EDGES = foreach GRAPH generate src, des;
X = join EDGES by src, IN_OUT_DEGREE BY node;
EDGES = foreach X generate
    EDGES::src as src,
    EDGES::des as des,
    IN_OUT_DEGREE::in_degree  as src_in_degree,
    IN_OUT_DEGREE::out_degree as src_out_degree;

-- join des by in-degree/out-degree
X = join EDGES by des, IN_OUT_DEGREE by node;
EDGES = foreach X generate
    EDGES::src as src,
    EDGES::des as des,
    EDGES::src_in_degree      as src_in_degree,
    EDGES::src_out_degree     as src_out_degree,
    IN_OUT_DEGREE::in_degree  as des_in_degree, 
    IN_OUT_DEGREE::out_degree as des_out_degree;
store EDGES into '$OUT/edges_w_in_out_degree';

-- compute correlation, this may be dangerous
DEGREES = foreach EDGES generate (DOUBLE)src_in_degree, (DOUBLE)src_out_degree, (DOUBLE)des_in_degree, (DOUBLE)des_out_degree;
X = group DEGREES all;
DEGREE_ASSOCIATION = FOREACH X GENERATE COR(DEGREES.$0, DEGREES.$1, DEGREES.$2, DEGREES.$3);
store DEGREE_ASSOCIATION into '$OUT/degree_association';







