/*
-- compute stats for undirected graph
-- assuming the undirected graph just store each edge once.

-- We keep two versions of the undirected graph
-- UNDIRECTED_GRAPH is the graph pairs with each edge represented only once
-- MUTUAL_GRAPH is the graph pairs with each edge represented twice
*/

%default GRAPH 'in/graph';
%default OUT 'out';

UNDIRECTED_GRAPH = load '$GRAPH' as (src:chararray, des:chararray, value:float);
UNDIRECTED_GRAPH = foreach UNDIRECTED_GRAPH generate src, des;
REVERSE_GRAPH    = foreach UNDIRECTED_GRAPH generate des as src, src as des;

MUTUAL_GRAPH     = UNION UNDIRECTED_GRAPH, REVERSE_GRAPH;

/*
-- compute the undirected graph from a directed graph
DIRECTED_GRAPH   = load '$DIRECTED_GRAPH' as (src:chararray, des:chararray, value:float);
------------------------------------------------------------
-- finding the reciprocial graph
------------------------------------------------------------
REVERSE_GRAPH = foreach DIRECTED_GRAPH generate
    des as src,
    src as des;
    
X = JOIN DIRECTED_GRAPH BY (src, des),  REVERSE_GRAPH BY (src, des);

-- Note that the mutual graph keep each edge TWICE
MUTUAL_GRAPH = foreach X generate
    DIRECTED_GRAPH::src as src,
    DIRECTED_GRAPH::des as des;

store MUTUAL_GRAPH into '$OUT/mutual_graph';

UNDIRECTED_GRAPH = filter MUTUTAL_GRAPH by src < des;

------------------------------------------------------------
-- compute reciprocity
------------------------------------------------------------
X = group MUTUAL_GRAPH all;
NUM_MUTUAL_EDGES = foreach X generate COUNT(MUTUAL_GRAPH)/2 as NUM_EDGES;
store NUM_MUTUAL_EDGES into '$OUT/num_mutual_edges';
*/

------------------------------------------------------------
-- compute degree distribution
------------------------------------------------------------
X = group MUTUAL_GRAPH by src;
DEGREE = foreach X generate group as node, COUNT(MUTUAL_GRAPH) as degree;
store DEGREE into '$OUT/degree';

------------------------------------------------------------
-- compute degree association
------------------------------------------------------------
EDGES = FOREACH MUTUAL_GRAPH GENERATE src, des;
X = join EDGES by src, DEGREE BY node;
EDGES = foreach X generate
    EDGES::src as src,
    EDGES::des as des,
    DEGREE::degree as src_degree;
X  = join EDGES by des, DEGREE BY node;
EDGES = foreach X generate
    EDGES::src as src,
    EDGES::des as des,
    EDGES::src_degree as src_degree,
    DEGREE::degree    as des_degree;
store EDGES into '$OUT/edges_w_degree';

-- compute correlation
DEGREES = foreach EDGES generate (double)src_degree, (double)des_degree;
X = group DEGREES all;
DEGREE_ASSOCIATION = foreach X generate COR(DEGREES.src_degree, DEGREES.des_degree);
store DEGREE_ASSOCIATION into '$OUT/degree_association';

------------------------------------------------------------
-- enumerate triangles, each triangle is stored in triplets
-- with node ids sorted
------------------------------------------------------------
A = filter MUTUAL_GRAPH by src < des;
B = filter MUTUAL_GRAPH by src < des;

TRIAD_JOIN = JOIN A by des, B by src;
OPEN_EDGES = foreach TRIAD_JOIN generate
    A::src as src,
    A::des as in_node,
    B::des as des;
TRIANGLE_JOIN = JOIN A by (src, des),  OPEN_EDGES by (src, des);
TRIANGLES = foreach TRIANGLE_JOIN generate
    OPEN_EDGES::src     as src,
    OPEN_EDGES::in_node as in_node,
    OPEN_EDGES::des     as des;
store TRIANGLES into '$OUT/triangles';

------------------------------------------------------------
-- compute #triangles associated with each node
------------------------------------------------------------
A = foreach TRIANGLES generate src     as node;
B = foreach TRIANGLES generate des     as node;
C = foreach TRIANGLES generate in_node as node;

NODE_TRIANGLES = union A, B, C;
X = group NODE_TRIANGLES by node;
NODE_NUM_TRIANGLES = foreach X generate
    group as node,
    COUNT(NODE_TRIANGLES) as num_triangles;
STORE NODE_NUM_TRIANGLES into '$OUT/node_num_triangles';

------------------------------------------------------------
-- compute clustering coefficient
------------------------------------------------------------
X = join NODE_NUM_TRIANGLES by node RIGHT OUTER, DEGREE by node;
Y = foreach X generate
    DEGREE::node as node,
    NODE_NUM_TRIANGLES::num_triangles as num_triangles,
    DEGREE::degree as degree;

Y = foreach Y generate node, degree, 
    ( num_triangles is null ? 0 : num_triangles) as num_triangles;

NODE_CLUSTERING_COEFFICIENT  = foreach Y generate
    node, degree,
    ( (degree < 2) ? 0 : num_triangles / ( (degree-1) * degree / 2.0 ) ) as clustering_coefficient;
store NODE_CLUSTERING_COEFFICIENT into '$OUT/node_clustering_coefficient';

CLUSTERING_COEFFICIENTS = foreach NODE_CLUSTERING_COEFFICIENT generate clustering_coefficient;
X = GROUP CLUSTERING_COEFFICIENTS all;
AVG_CLUSTERING_COEFFICIENT = foreach X generate AVG(CLUSTERING_COEFFICIENTS.clustering_coefficient);
store AVG_CLUSTERING_COEFFICIENT into '$OUT/avg_clustering_coefficient';


