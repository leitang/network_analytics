function [cc,c,d] = compute_clustering_coefficient(a)
% [cc,c,d] = compute_clustering_coefficient(A) 
% - clustering coefficients of a symmetric adjacency matrix
%
% A:  should be a symmetric adjacency matrix
%     please make sure the diagonal entries are zero (no self link)
% cc: average local clustering coefficient 
% c : the clustering efficient for each node
% d : degree

% test symmetry
issym = size(a,1)==size(a,2) && isempty(find(a-a'));
if ~issym
    error('CLUSTCOEFF requires symmetric input matrix');
end

% local clustering coefficient
d = sum(a,2); % degree of each node
cn = diag(a*triu(a)*a); % number of triangles for each node
c = zeros(size(d));
c(d>1) = 2*cn(d>1)./(d(d>1).*(d(d>1)-1));
%%cc = mean(c(d>1));
cc = mean(c);

% if nargout==2
%     % global clustering coefficient
%     b = triu(a*a,1);
%     c2 = sum(b); % number of connected triples
%     c1 = sum(b.*a); % 3x number of triangles
%     varargout{2} = full(sum(c1)/sum(c2)); % clustering coefficient 2
% end

