-- convert graph triplets into adjacency list, keyed by columns
-- add self loop
-- convert into Markov matrix (each column sums up to 1)

DIRECTED_GRAPH = load '$GRAPH' as (src:chararray, des:chararray, weight:double);

X = GROUP DIRECTED_GRAPH by src;
foreach X {
    neighbors = foreach DIRECTED_GRAPH generate des, weight;
    -- append self loop


};


-- expansion
M = matrix_multiplication(M, M);

-- inflation
M = inflate(M, '$r');

-- prune
M = filter M by 

define prune(TRIPLET_A, THRESHOLD) returns TRIPLET_M {
    $TRIPLET_M  = filter $TRIPLET_A by weight 
};


define inflate(TRIPLET_A, LOG_R) returns TRIPLET_M {
    TRIPLET_A = foreach $TRIPLET_A generate $0 as index_i, $1 as index_j, $2 as weight;
    LOG_A = foreach TRIPLET_A generate
        index_i,
        index_j, 
        LOG(weight) + $LOG_R as log_weight;
    $TRIPLET_M = foreach LOG_A GENERATE index_i, index_j, EXP(log_weight) as weight;
};

-- assume both A and B are triplets, compute M = A * B
define matrix_multiplication (TRIPLET_A, TRIPLET_B) returns TRIPLET_M {
    TRIPLET_A = foreach $TRIPLET_A generate $0 as index_i, $1 as index_k, $2 as weight;
    TRIPLET_B = foreach $TRIPLET_B generate $1 as index_k, $1 as index_j, $2 as weight;

    X = join TRIPLET_A by index_k, TRIPLET_B by index_k using 'skewed';
    IJ_PAIRS = foreach X generate
        TRIPLET_A::index_i as index_i,
        TRIPLET_B::index_j as index_j,
        TRIPLET_A::weight * TRIPLET_B as weight;

    Y  = group IJ_PAIRS by (index_i, index_j);
    $TRIPLET_M = foreach Y generate
        flatten(group) as (index_i, index_j),
        SUM(IJ_PAIRS.weight) as weight;
};

-- take triplets as input, return stochastic matrix in triplets
define convert_into_stochastic_matrix (TRIPLETS) returns TRIPLET_M {
    TRIPLETS = foreach $TRIPLETS GENERATE $0 as index_i, $1 as index_j, $2 as weight;
    X = group TRIPLETS by index_j;
    Y = foreach X generate TRIPLETS as tri_bag, SUM(TRIPLETS.weight) as total_weight;
    Z = foreach Y generate flatten(tri_bag) as  (index_i, index_j, weight), total_weight;
    $TRIPLET_M = foreach Z generate index_i, index_j, weight / total_weight as weight;
};

