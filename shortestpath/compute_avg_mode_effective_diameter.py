#! /usr/bin/python
# compute the average, mode and effective diameter (90%) of the shortest paths

import fileinput
import re

def main():
    delimeter = re.compile("\s+")
    distr = {}
    for line in fileinput.input('-'):
        splits = delimeter.split( line.rstrip("\n") )
        length, num_paths = int(splits[0]), int(splits[1])
        distr[length] = num_paths

    # compute the average
    total_paths  = sum(distr.values())
    total_length = sum( [k*v for k, v in distr.items()])
    avg_length = float(total_length) / total_paths

    # compute the accumulative distr
    cdf = [(0,0)]
    cumulative_paths = 0.0
    for length, num_paths in distr.iteritems():
        cumulative_paths += num_paths
        cdf.append( (length,  cumulative_paths / total_paths ) )

    # find the mode
    mode_length = find_point(0.5, cdf)
    
    # find the 90% effective shortest path
    effective_length = find_point(0.9, cdf)

    print avg_length, mode_length, effective_length
    print cdf
    

def find_point(prob_point, cdf):

    pre_pair = (0, 0)
    for length, prob in cdf:
        if prob > prob_point:
            cur_pair = (length, prob)
            break
        else:
            pre_pair = (length, prob)


    pre_length, pre_prob = pre_pair
    cur_length, cur_prob = cur_pair

    ratio = ( prob_point - pre_prob ) / (cur_prob - pre_prob)
    length_point = pre_length + (cur_length - pre_length) * ratio
    return length_point
    
    
main()
    

        