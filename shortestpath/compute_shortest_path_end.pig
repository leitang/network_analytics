-- compute shortest path quantile

PAIR_DISTANCE = load '$PAIR_DISTANCE' as (src:chararray, des:chararray, dist:int);
PAIR_DISTANCE = filter PAIR_DISTANCE by src neq des;

X = group PAIR_DISTANCE by dist;
PATH_DISTR = foreach X generate group as dist,
    COUNT(PAIR_DISTANCE) as num_paths;
store PATH_DISTR into '$OUT/path_distr';

-- the mode, avg, median and 90% quantile can be computed offline

