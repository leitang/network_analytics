/*
initilaization for shortest path
*/
%default GRAPH 'graph';
%default SAMPLE_RATE '0.000001';
%default OUT 'out';

GRAPH = LOAD '$GRAPH' as (src:chararray, des:chararray);
X = group GRAPH by src;
ADJ_LIST = foreach X GENERATE group as src, GRAPH.des as neighbors;
store ADJ_LIST into '$OUT/adj_list';


SEEDS = SAMPLE ADJ_LIST $SAMPLE_RATE;
FRONTIER = foreach SEEDS GENERATE
    src as seed,
    flatten(neighbors) as des,
    1 as dist;
store FRONTIER INTO '$OUT/frontier';
