-- propagate
%default PAIR_DISTANCE 'pair_distance';
%default FRONTIER 'frontier';
%default ADJ_LIST 'adj_list';
%default OUT 'out';


PAIR_DISTANCE = load '$PAIR_DISTANCE' as (seed:chararray, des:chararray, dist:int);
FRONTIER = load '$FRONTIER' as (seed:chararray, des:chararray, dist:int);

ADJ_LIST = load '$ADJ_LIST' as (node:chararray, neighbors:bag{(neighbor::chararray)});

X = join FRONTIER by des, ADJ_LIST by node;
WAVE = foreach X generate
    FRONTIER::seed as seed,
    FLATTEN(ADJ_LIST::neighbors) as des, 
    FRONTIER::dist+1 as dist;

WAVE = filter WAVE by seed neq des;

X = group WAVE by (seed, des);
NEW_ADD_ON = foreach X{
    generate flatten(group) as (seed, des), MIN(WAVE.dist) as dist;
};

Y = join PAIR_DISTANCE BY (seed, des) FULL OUTER, NEW_ADD_ON by (seed, des);

OLD_PAIRS =  FILTER Y BY PAIR_DISTANCE::seed is not null;
NEW_PAIRS =  FILTER Y by PAIR_DISTANCE::seed is null;

OLD_PAIRS = foreach OLD_PAIRS generate
    PAIR_DISTANCE::seed as seed,
    PAIR_DISTANCE::des  as des,
    PAIR_DISTANCE::dist as dist;

NEW_PAIRS = foreach NEW_PAIRS generate
    NEW_ADD_ON::seed as seed,
    NEW_ADD_ON::des  as des,
    NEW_ADD_ON::dist as dist;

NEW_PAIR_DISTANCE = union OLD_PAIRS, NEW_PAIRS;
STORE NEW_PAIRS into '$OUT/frontier';
STORE NEW_PAIR_DISTANCE into '$OUT/pair_distance';

