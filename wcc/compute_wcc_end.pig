/*
this script takes into an undirected graph, and compute the connected components
this is done through brute-force BFS,
each node carries a token, which is the min(neighborhood_ids)


*/
%default NODE_W_TOKEN 'node_w_token';
%default OUT 'out';
    
NODE_W_TOKEN = load '$NODE_W_TOKEN' as (node:chararray, token:chararray, updated:int);

NODE_COMPONENT = foreach NODE_W_TOKEN generate node, token as component;

X = group NODE_COMPONENT by component;

COMPONENT_SIZE = foreach X generate group as component, COUNT(NODE_COMPONENT) as component_size;

STORE COMPONENT_SIZE into '$OUT/component_size';
STORE NODE_COMPONENT into '$OUT/node_component';
