/*
this script takes into an undirected graph, and compute the connected components
this is done through brute-force BFS,
each node carries a token, which is the min(neighborhood_ids)
*/
%default GRAPH 'in/graph';
%default OUT 'out';

    
UNDIRECTED_GRAPH = load '$GRAPH' as (src:chararray, des:chararray);
REVERSE_GRAPH = foreach UNDIRECTED_GRAPH  generate des as src, src as des;
MUTUAL_GRAPH = DISTINCT ( union UNDIRECTED_GRAPH, REVERSE_GRAPH );

-- convert into adjacency list
X = GROUP MUTUAL_GRAPH BY src;
ADJ_LIST = foreach X generate group as node, MUTUAL_GRAPH.des as neighbors;
store ADJ_LIST into '$OUT/adj_list';

-- INITIALIZATION
NODE_W_TOKEN = foreach ADJ_LIST generate node, MIN(neighbors) as token, 1 as updated;
NODE_W_TOKEN = foreach NODE_W_TOKEN generate node,
   ( node < token ? node : token ) as token,
    updated;
store NODE_W_TOKEN into '$OUT/node_w_token';

