/*
this script takes into an undirected graph, and compute the connected components
this is done through brute-force BFS,
each node carries a token, which is the min(neighborhood_ids)
*/
%default NODE_W_TOKEN 'node_w_token';
%default ADJ_LIST 'adj_list';
%default OUT 'out';
    
NODE_W_TOKEN = load '$NODE_W_TOKEN' as (node:chararray, token:chararray, updated:int);
ADJ_LIST     = load '$ADJ_LIST'     as (node:chararray, neighbors:bag{(des:chararray)});
    
-- ITERATIVE PROCESS
-- only propagate if the token is updated
UPDATED_NODES = filter NODE_W_TOKEN by updated > 0;
X = join UPDATED_NODES by node, ADJ_LIST by node;
-- propagete the token
A = foreach X generate
    FLATTEN(ADJ_LIST::neighbors) as node,
    UPDATED_NODES::token as token,
    0 as from_self;

-- emit the self token as well
B = foreach NODE_W_TOKEN generate node, token, 1 as from_self;

-- keep info of the previous token
TOKENS = union A, B;
X = GROUP TOKENS by node;
NODE_W_UPDATED_TOKEN = foreach X  {
    D = filter TOKENS by from_self > 0;
    cur_tokens = foreach D generate token;
    generate group as node, MIN(TOKENS.token) as token, FLATTEN(cur_tokens.token) as old_token;
};

NODE_W_UPDATED_TOKEN = FOREACH NODE_W_UPDATED_TOKEN GENERATE
    node, token, ( (token eq old_token) ? 0 : 1 ) as updated;
store NODE_W_UPDATED_TOKEN into '$OUT/node_w_token';

UPDATED_NODES = FILTER NODE_W_UPDATED_TOKEN by updated > 0;
X = GROUP UPDATED_NODES all parallel 1;
NUM_UPDATED = foreach X generate COUNT(UPDATED_NODES);
store NUM_UPDATED into '$OUT/num_updated';


